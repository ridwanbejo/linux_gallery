// importing modules here
var users = require('../models/users');
var rating = require('../models/rating');

var js_objects = require('../helpers/js_objects');

// local variable here
exports.index = function (response, request) {
	console.log('user.index method is executed.........');
	if (users.auth()){
		
		users.all(function(rows){
			response.writeHeader(200, {"Content-Type":"application/json"});
			data = {
				'error_code' : 200,
				'message': 'Retrieve query success',
				'results': rows
			};
			response.write(JSON.stringify(data));
			response.end();
		});

	}
	else {
		response.writeHeader(200, {"Content-Type":"application/json"});
		
		response.write(
							JSON.stringify({'error_code':403, 
								'message': 'You are not authorized...'
							}) 
						);
		
		response.end();
		
	}

}

exports.auth = function (response, request) {
	console.log('user.auth method is executed...');

	response.writeHeader(200, {"Content-Type":"text/html"});
	response.write('user.auth method is executed...');
	response.end();
}

exports.register = function (response, request) {
	console.log('user.register method is executed...');

	response.render('index.html', {name:'ridwanbejo', email:'ridwanbejo@gmail.com'});

	if (users.auth()){
		
		fields = '';

		post_length = js_objects.object_len(request.POST);
		
		var c = 0;
		for (i in request.POST){
			c++;

			if (i == 'password'){
				fields += 'md5("'+request.POST[i]+'")';
			}
			else if (i == 'username' || i == 'email' ){
				fields += ' "'+request.POST[i]+'"';
			}
			else {
				fields += request.POST[i];
			}	

			if (c != post_length){
				fields += ", ";
			}
			
		}

		users.create(fields, function(rows, error){
			response.writeHeader(200, {"Content-Type":"application/json"});
			
			if (error == null){
				data = {
					'error_code' : 200,
					'message': 'Register user success',
					'results': rows
				};
			}
			else {
				data = {
					'error_code' : 500,
					'message': 'Register user failed',
					'error': error,
				};	
			}

			response.write(JSON.stringify(data));
			response.end();
		});

	}
	else {
		response.writeHeader(200, {"Content-Type":"application/json"});
		
		response.write(
							JSON.stringify({'error_code':403, 
								'message': 'You are not authorized...'
							}) 
						);
		
		response.end();
		
	}
}

exports.edit = function (response, request) {
	if (users.auth()){
		
		fields = '';

		post_length = js_objects.object_len(request.POST);
		
		var c = 0;
		for (i in request.POST){
			c++;

			if (i == 'password'){
				fields += ' '+i+'=md5("'+request.POST[i]+'") ';
			}
			else {
				fields += ' '+i+'="'+request.POST[i]+'" ';
			}	

			if (c != post_length){
				fields += ", ";
			}
			
		}

		users.edit(request.PARAM[0], fields, function(rows, error){
			response.writeHeader(200, {"Content-Type":"application/json"});
			if (error == null) {
				data = {
					'error_code' : 200,
					'message': 'Retrieve query success',
					'error_message':error,
					'results': rows
				};

			}
			else {
				data = {
					'error_code' : 500,
					'message': 'Retrieve query failed',
					'error': error,
				};	
			}
			response.write(JSON.stringify(data));
			response.end();
		});

	}
	else {
		response.writeHeader(200, {"Content-Type":"application/json"});
		
		response.write(
							JSON.stringify({'error_code':403, 
								'message': 'You are not authorized...'
							}) 
						);
		
		response.end();
		
	}
}

exports.detail = function (response, request) {
	
	if (users.auth()){
		
		users.detail(request.PARAM[0], function(rows, err){
			response.writeHeader(200, {"Content-Type":"application/json"});

			if (err == null){
				data = {
					'error_code' : 200,
					'message': 'Retrieve query success',
					'results': rows
				};
			}
			else {
				data = {
					'error_code' : 500,
					'message': 'Retrieve query failed',
					'error': err,
				};	
			}
			
			response.write(JSON.stringify(data));
			response.end();
		});

	}
	else {
		response.writeHeader(200, {"Content-Type":"application/json"});
		
		response.write(
							JSON.stringify({'error_code':403, 
								'message': 'You are not authorized...'
							}) 
						);
		
		response.end();
		
	}
}

exports.delete = function (response, request) {

	response.writeHeader(200, {"Content-Type":"text/html"});
	response.write('user.delete method is executed...');
	response.end();

}

exports.get_rated_item = function (response, request){
	console.log('get_rated_item executed...');
	if (users.auth()){
		
		rating.get_rated_item_by_users(request.PARAM[0], request.PARAM[1], function(rows, err){
			response.writeHeader(200, {"Content-Type":"application/json"});
			
			if (err == null){
				data = {
					'error_code' : 200,
					'message': 'Retrieve query success',
					'results': rows,
				};
			}
			else {
				data = {
					'error_code' : 500,
					'message': 'Retrieve query failed',
					'error': err,
				};	
			}

			response.write(JSON.stringify(data));
			response.end();
		});

	}
	else {
		response.writeHeader(200, {"Content-Type":"application/json"});
		
		response.write(
							JSON.stringify({'error_code':403, 
								'message': 'You are not authorized...'
							}) 
						);
		
		response.end();
		
	}
}