// importing modules here
var users = require('../models/users');
var distro = require('../models/distro');
var rating = require('../models/rating');

var js_objects = require('../helpers/js_objects');

// local variable here

exports.index = function (response, request) {
	console.log('user.index method is executed.........');
	if (users.auth()){
		
		distro.all(function(rows, err){
			response.writeHeader(200, {"Content-Type":"application/json"});
			
			if (err == null){
				data = {
					'error_code' : 200,
					'message': 'Retrieve query success - distro index',
					'results': rows,
				};
			}
			else {
				data = {
					'error_code' : 500,
					'message': 'Retrieve query failed',
					'error': err,
				};	
			}

			response.write(JSON.stringify(data));
			response.end();
		});

	}
	else {
		response.writeHeader(200, {"Content-Type":"application/json"});
		
		response.write(
							JSON.stringify({'error_code':403, 
								'message': 'You are not authorized...'
							}) 
						);
		
		response.end();
		
	}

}

exports.create = function (response, request) {

	if (users.auth()){
		
		console.log(request.POST);

		fields = '';

		post_length = js_objects.object_len(request.POST);
		
		var c = 0;
		for (i in request.POST){
			c++;

			if (i == 'name' || i == 'origin'  || i == 'creator' || i == 'description' || i == 'first_release_date'){
				fields += ' "'+request.POST[i]+'"';
			}
			else {
				fields += request.POST[i];
			}	

			if (c != post_length){
				fields += ", ";
			}
			
		}

		distro.create(fields, function(rows, error){
			response.writeHeader(200, {"Content-Type":"application/json"});
			
			if (error == null){
				data = {
					'error_code' : 200,
					'message': 'Create distro is success...',
					'results': rows
				};

			}
			else {
				data = {
					'error_code' : 500,
					'message': 'Create distro is failed',
					'error': error,
				};	
			}

			response.write(JSON.stringify(data));
			response.end();
		});

	}
	else {
		response.writeHeader(200, {"Content-Type":"application/json"});
		
		response.write(
							JSON.stringify({'error_code':403, 
								'message': 'You are not authorized...'
							}) 
						);
		
		response.end();
		
	}
}

exports.destroy = function (response, request) {
	
	if (users.auth()){
		
		distro.destroy (request.PARAM[0], function(rows, err){
			response.writeHeader(200, {"Content-Type":"application/json"});
			if (err == null){
				data = {
					'error_code' : 200,
					'message': 'Delete distro is success',
					'results': rows
				};
			}
			else {
				data = {
					'error_code' : 500,
					'message': 'Delete distro is failed',
					'error': err,
				};	
			}
			response.write(JSON.stringify(data));
			response.end();
		});

	}
	else {
		response.writeHeader(200, {"Content-Type":"application/json"});
		
		response.write(
							JSON.stringify({'error_code':403, 
								'message': 'You are not authorized...'
							}) 
						);
		
		response.end();
		
	}

}

exports.detail = function (response, request) {
	
	if (users.auth()){
		
		distro.detail(request.PARAM[0], function(rows, err){
			response.writeHeader(200, {"Content-Type":"application/json"});
			
			if (err == null) {
				data = {
					'error_code' : 200,
					'message': 'Retrieve query success',
					'results': rows
				};
			} else {
				data = {
					'error_code' : 500,
					'message': 'Retrieve query failed',
					'error': err,
				};	
			}

			response.write(JSON.stringify(data));
			response.end();
		});
	}
	else {
		response.writeHeader(200, {"Content-Type":"application/json"});
		
		response.write(
							JSON.stringify({'error_code':403, 
								'message': 'You are not authorized...'
							}) 
						);
		
		response.end();
		
	}
}

exports.edit = function (response, request) {

	if (users.auth()){
		
		console.log(request.POST);

		fields = '';

		post_length = js_objects.object_len(request.POST);
		
		var c = 0;
		for (i in request.POST){
			c++;

			if (i == 'name' || i == 'origin'  || i == 'creator' || i == 'description' || i == 'first_release_date'){
				fields += ' '+i+'= "'+request.POST[i]+'"';
			}
			else {
				fields += ' '+i+'= '+request.POST[i];
			}	

			if (c != post_length){
				fields += ", ";
			}
			
		}

		distro.edit(request.PARAM[0], fields, function(rows, error){
			response.writeHeader(200, {"Content-Type":"application/json"});
			
			if (error == null){
				data = {
					'error_code' : 200,
					'message': 'Update distro success...',
					'results': rows
				};
			}
			else {
				data = {
					'error_code' : 500,
					'message': 'Update distro failed',
					'error': error,
				};	
			}
			response.write(JSON.stringify(data));
			response.end();
		});

	}
	else {
		response.writeHeader(200, {"Content-Type":"application/json"});
		
		response.write(
							JSON.stringify({'error_code':403, 
								'message': 'You are not authorized...'
							}) 
						);
		
		response.end();
		
	}
}

exports.get_rating = function (response, request){
	if (users.auth()){
		
		rating.get_rating_by_item(request.PARAM[0], 'distro', function(rows, err){
			response.writeHeader(200, {"Content-Type":"application/json"});
			
			if (err == null){
				data = {
					'error_code' : 200,
					'message': 'Retrieve query success',
					'results': rows,
				};
			}
			else {
				data = {
					'error_code' : 500,
					'message': 'Retrieve query failed',
					'error': err,
				};	
			}

			response.write(JSON.stringify(data));
			response.end();
		});

	}
	else {
		response.writeHeader(200, {"Content-Type":"application/json"});
		
		response.write(
							JSON.stringify({'error_code':403, 
								'message': 'You are not authorized...'
							}) 
						);
		
		response.end();
		
	}
}

exports.rating = function (response, request) {
	
	if (users.auth()){
		
		fields = request.POST;

		rating.set_rating(request.PARAM[0], fields, function(rows, err){
			response.writeHeader(200, {"Content-Type":"application/json"});
			if (err == null){
				data = {
					'error_code' : 200,
					'message': 'Set rating success',
					'results': rows
				};
			}
			else {
				data = {
					'error_code' : 500,
					'message': 'Set rating failed',
					'error': err,
				};	
			}
			response.write(JSON.stringify(data));
			response.end();
		});

	}
	else {
		response.writeHeader(200, {"Content-Type":"application/json"});
		
		response.write(
							JSON.stringify({'error_code':403, 
								'message': 'You are not authorized...'
							}) 
						);
		
		response.end();
		
	}
}

exports.destroy_rating = function (response, request) {
	
	if (users.auth()){
		
		fields = request.POST;

		rating.remove(request.PARAM[0], function(rows, err){
			response.writeHeader(200, {"Content-Type":"application/json"});
			if (err == null){
				data = {
					'error_code' : 200,
					'message': 'Remove rating success',
					'results': rows
				};
			}
			else {
				data = {
					'error_code' : 500,
					'message': 'Remove rating failed',
					'error': err,
				};	
			}
			response.write(JSON.stringify(data));
			response.end();
		});

	}
	else {
		response.writeHeader(200, {"Content-Type":"application/json"});
		
		response.write(
							JSON.stringify({'error_code':403, 
								'message': 'You are not authorized...'
							}) 
						);
		
		response.end();
		
	}
}