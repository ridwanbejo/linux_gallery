var mysql = require('mysql');

exports.all = function (callback) {

	db_connection = mysql.createConnection(global.connection_config);

	sql_string = "SELECT md5(concat(id_distro, '*linux_gallery')) as id_distro,  name, origin, creator, rating, first_release_date, predecessor FROM distro";
	
	db_connection.query(sql_string, function(err, rows, fields){
		callback(rows, err);
	});

	db_connection.end();
}

exports.create = function (fields, callback){
	db_connection = mysql.createConnection(global.connection_config);

	sql_string = "INSERT INTO distro (name, origin, creator, predecessor, description, first_release_date, rating) values ("+fields+", 0)";
	console.log(sql_string);
	db_connection.query(sql_string, function(err, rows, fields){
		callback(rows, err);
	});

	db_connection.end();
}

exports.destroy = function (id, callback){
	// contoh parameter: 90aed438344d30321a63b7eca6d4d5ce
	console.log('di dalam model...');

	db_connection = mysql.createConnection(global.connection_config);

	sql_string = "DELETE FROM distro where md5(concat(id_distro, '*linux_gallery')) = '"+id+"'";
	
	db_connection.query(sql_string, function(err, rows, fields){
		callback(rows, err);
	});

	db_connection.end();

}

exports.detail = function (id, callback){
	// contoh parameter: 90aed438344d30321a63b7eca6d4d5ce

	db_connection = mysql.createConnection(global.connection_config);

	sql_string = "SELECT *, md5(concat(id_distro, '*linux_gallery')) as id_distro, md5(concat(predecessor, '*linux_gallery')) as predecessor FROM distro where md5(concat(id_distro, '*linux_gallery')) = '"+id+"'";
	
	db_connection.query(sql_string, function(err, rows, fields){
		console.log(err);
		callback(rows, err);
	});

	db_connection.end();

}

exports.edit = function (id, fields, callback){
		
	db_connection = mysql.createConnection(global.connection_config);

	sql_string = "UPDATE distro set "+fields+" where md5(concat(id_distro, '*linux_gallery')) = '"+id+"'";
	console.log(sql_string);
	db_connection.query(sql_string, function(err, rows, fields){
		callback(rows, err);
	});

	db_connection.end();
}

