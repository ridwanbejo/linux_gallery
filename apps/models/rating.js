var mysql = require('mysql');
var Q = require('q');

exports.set_rating = function (id, fields, callback){
	
	fields_insert = '';
	fields_insert += " (SELECT id FROM users where md5(concat(id, '*linux_gallery')) = '"+fields.users_id+"' ), "
	if (fields.type == 'distro'){
		fields_insert += " (SELECT id_distro FROM distro where md5(concat(id_distro, '*linux_gallery')) = '"+fields.type_id+"' ), "
	}	
	fields_insert += " '"+fields.type+"'"

	db_connection = mysql.createConnection(global.connection_config);

	sql_string = "SELECT count (id_rating) as has_rating FROM rating where md5(concat(users_id, '*linux_gallery')) = '"+fields.users_id+"' and md5(concat(type_id, '*linux_gallery')) = '"+fields.type_id+"' and type='"+fields.type+"'";
	console.log(sql_string);

	db_connection.query(sql_string, function(err, rows, fields){
		console.log('check  rating sudah beres...');
		if ( rows[0].has_rating  <= 0 ) {
			// insert dulu ke tabel rating dengan tipe rate distro
			db_connection2 = mysql.createConnection(global.connection_config);

			sql_string = "INSERT INTO rating (users_id, type_id, type) values ("+fields_insert+")";
			console.log(sql_string);

			db_connection2.query(sql_string, function(err, rows, fields){
				console.log('insert rating beres...');
			});

			db_connection2.end(function(err){
				// baru update rating di tabel distro	
				db_connection3 = mysql.createConnection(global.connection_config);

				sql_string = "UPDATE distro set rating = "+
							 "(SELECT count(id_rating) from rating where type='distro' and md5(concat(id_distro, '*linux_gallery')) = '"+id+"') "+
							 "where md5(concat(id_distro, '*linux_gallery')) = '"+id+"'";
				console.log(sql_string);
				
				db_connection3.query(sql_string, function(err, rows, fields){
					console.log('update rating beres...');
					callback(rows, err);
				});

				db_connection3.end();
			});
		}	
		else {
			callback([], 'Users has rated this item...');
		}
	});

	db_connection.end();

	
}

exports.get_rating_by_item = function (id, item_type, callback){
	db_connection = mysql.createConnection(global.connection_config);

	sql_string = "SELECT * FROM rating where md5(concat(type_id, '*linux_gallery')) = '"+id+"' and type='"+item_type+"'";
	
	db_connection.query(sql_string, function(err, rows, fields){
		callback(rows, err);
	});

	db_connection.end();
}

exports.get_rated_item_by_users = function (id, item_type, callback){
	db_connection = mysql.createConnection(global.connection_config);

	sql_string = "SELECT * FROM rating where md5(concat(users_id, '*linux_gallery')) = '"+id+"' and type='"+item_type+"'";
	
	db_connection.query(sql_string, function(err, rows, fields){
		callback(rows, err);
	});

	db_connection.end();
}

exports.remove = function (id, callback){
	
	db_connection = mysql.createConnection(global.connection_config);

	sql_string = "DELETE FROM rating where md5(concat(id_rating, '*linux_gallery')) = '"+id+"'";
	
	db_connection.query(sql_string, function(err, rows, fields){
		callback(rows, err);
	});

	db_connection.end();
}