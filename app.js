var http = require('http');
var url = require('url');
var qs = require('querystring');
var swig = require('swig');

// server variable
var executor_path = './apps/controllers/';
var views_path = './apps/views/';
var server_port = 8084;

// list of url
var urls = [

	{'method':'GET',  'url': /\/users$/,  'executor':'user.index' },
	{'method':'GET',  'url': /\/distro$/,  'executor':'distro.index' },
	
	{'method':'POST',  'url': /\/users\/register$/,  'executor' : 'user.register' },
	{'method':'POST',  'url': /\/users\/auth$/,  'executor'  : 'user.auth' },
	
	{'method':'PUT',  'url': /\/users\/[0-9a-z]{32}/,  'executor'   : 'user.edit' },
	{'method':'GET',  'url': /\/users\/[0-9a-z]{32}$/,  'executor'   : 'user.detail' },
	{'method':'GET',  'url': /\/users\/[0-9a-z]{32}\/rated\/[distro]/,  'executor'   : 'user.get_rated_item' },
	
	{'method':'POST',  'url': /\/distro\/new$/,  'executor' : 'distro.create' },		
	{'method':'DELETE',  'url': /\/distro\/[0-9a-z]{32}$/,  'executor' : 'distro.destroy' },
	{'method':'GET',  'url': /\/distro\/[0-9a-z]{32}$/,  'executor'   : 'distro.detail' },
	{'method':'PUT',  'url': /\/distro\/[0-9a-z]{32}/,  'executor'   : 'distro.edit' },
	
	{'method':'POST',  'url': /\/distro\/[0-9a-z]{32}\/rating$/,  'executor'   : 'distro.rating' },
	{'method':'GET',  'url': /\/distro\/[0-9a-z]{32}\/rating$/,  'executor'   : 'distro.get_rating' },
	{'method':'DELETE',  'url': /\/distro\/rating\/[0-9a-z]{32}$/,  'executor'   : 'distro.destroy_rating' },
				
];

// common configuration
global.connection_config = {
        host: 'localhost',
        user: 'root',
        password: 'root',
        database: 'linux_gallery',
        multipleStatements: true,
    };

// will show 404 page
function show_404(response){
	response.writeHeader(200, {"Content-Type":"text/html"});
	response.write('Not found...');
	response.end();
}

// will show static file
function show_static(response){
}

// checking for the incoming route
function check_route(request, urls, incoming_req_path){
	// pengecekan URL
	for  (idx in urls ){

		if (request.method == urls[idx].method ){
			match_url = urls[idx].url.exec(incoming_req_path.pathname); 

			if ( match_url != null ) {
				return urls[idx].executor.split('.');
				break;
			}
		}
	}

	return false;
}

function do_GET(request, incoming_req_path){

	return incoming_req_path.query;

}

function do_POST(request, response, callback){
	var postData = "";

	request.setEncoding("utf8");
	
	request.addListener("data", function(postDataChunk){
		postData += postDataChunk;
		postDataChunk + ".";
	});

	request.addListener("end", function(){
		temp_data = qs.parse(postData);
		request.POST = temp_data;
		
		callback(request, response);
	});
}

function do_PARAM(request, urls, incoming_req_path){
	param = {};
	i = 0;
	for  (idx in urls ){

		if (request.method == urls[idx].method ){
			match_url = urls[idx].url.exec(incoming_req_path.pathname); 
			if ( match_url != null ) {
				x = urls[idx].url.toString();
				p = x.replace(/\\\//g, '/');
				q = p.substr(1, p.length-2);
				m = q.split('/');
				req_path = incoming_req_path.pathname.split('/');

				// nanti setiap elemen dicheck apahkah sama, kalau sama berarti bukan param
				// kalau beda dicheck lagi dengan regex dari m terhadap req_path
				// jika hasilnya sesuai maka itu param, jika beda yah biarkan.
				
				for (idx2 in m ){
					if (m[idx2] != req_path[idx2]) {
						console.log(req_path[idx2]);
						param[i] = req_path[idx2];
						i++;
					}
				}
			}
		}
	}

	console.log(param);
	return param;
}

// main server code
var server = http.createServer(function(request, response){
	var incoming_req_path = url.parse(request.url, true);

	request.GET = do_GET(request, incoming_req_path);
	response.render = function (file_path, template_data){
							var tpl = swig.compileFile(views_path+file_path);
							
							response.writeHeader(200, {"Content-Type":"text/html"});
							response.write(
									tpl ( 
											template_data
										) 
								);
							response.end();
						};

	which_routes = check_route(request, urls, incoming_req_path);
	request.PARAM = do_PARAM(request, urls, incoming_req_path);

	if (typeof(which_routes) == 'object'){
		executor = which_routes;
		exec_modules = require(executor_path+executor[0]);
		exec_method = exec_modules[executor[1]];
		
		if (request.method != "GET"){
			console.log('hahaha');
			do_POST(request, response, function(){
				exec_method(response, request);
			});
		}
		else {
			exec_method(response, request);
		}
	}
	else {
		show_404(response);
	}

	console.log(request.method + " --- " + incoming_req_path.pathname);
});

// running the apps
server.listen(server_port);

console.log('server is running on port: '+server_port); 